/**
***********************************************************************************************************************
*
* jQuery Editable+ PlugIn
* Provides inline basic editing for elements like div, p, and span.
* 
* @author   David Simmons <dsimmonsesign@gmail.com>
* @version  0.0.1-dev
* @license  LGPLv3
* @see      http://dsimmonsdesign.com/projects/editable
*
***********************************************************************************************************************
*/
(function($){
    $.fn.editable = function(options){
        
        /**
        ---------------------------------------------------------------------------------------------------------------
        Default Plugin Options
        ---------------------------------------------------------------------------------------------------------------
        */
        var settings = $.extend({
            element_class:              'editable-element',      
            element_placeholder:        '...',        
            element_placeholder_icon:   '',
            editor_class:               'editable-editor',      
            editor_tag:                 'input',                
            editor_tag_expand:          0,
            editor_tag_options:         ['option'],              
            editor_maxlength:           0,                       
            callback_pre:               '',                      
            callback_post:              '',                      
            auto_class:                 1,
            auto_trigger:               0
        }, options);

        /**
        ---------------------------------------------------------------------------------------------------------------
        Declare Vars
        ---------------------------------------------------------------------------------------------------------------
        */
        var element_tag = this.get(0).tagName;
        var element_id  = this.get(0).id;
        var editor_id   = this.get(0).id + '-editable';
 
        /**
        ---------------------------------------------------------------------------------------------------------------
        Build Templates
        ---------------------------------------------------------------------------------------------------------------
        */
        var tpl_element = '<' + element_tag + ' id="' + element_id + '"></' + element_tag + '>';
        var tpl_editor  = '';
        switch(settings.editor_tag){
            default:
            case 'input':
               tpl_editor += '<input type="text" id="' + editor_id +'" ';
               tpl_editor += (settings.editor_maxlength != 0) ? 'maxlength="' + settings.editor_maxlength + '"' : '';
               tpl_editor += ' />';
            break;         
            case 'textarea':
                tpl_editor += '<textarea id="' + editor_id +'" ';
                tpl_editor += (settings.editor_maxlength != 0) ? 'maxlength="' + settings.editor_maxlength + '"' : '';
                tpl_editor += '></textarea>';
            break;
            case 'select':
                tpl_editor += '<select id="' + editor_id +'" onchange="this.blur()">';
                $.each(settings.editor_tag_options, function(index, value){
                    tpl_editor += '<option selected>' + value + '</option>';
                });
                tpl_editor += '</select>';
            break;
            case 'image':
               tpl_editor += '<input type="file" id="' + editor_id +'" />';               
            break;
        }


        /**
        ---------------------------------------------------------------------------------------------------------------
        Handle Element Click
        ---------------------------------------------------------------------------------------------------------------
        */
        $(document).on('click', '#' + element_id, function(){

            // get some properties of the element
            var content = $('#' + element_id).text();
            var h = $('#' + element_id).height();
            var w = $('#' + element_id).width();

            // if empty content (only placeholder is present) then reset content
            content = (content == settings.element_placeholder) ? '' : content;

            // if a callback function is registered then fire it off
            (settings.callback_pre != '') ? window[settings.callback_pre](content) : '';
            
            // replace the element with the editor template and apply classes
            $('#' + element_id).replaceWith(tpl_editor);
            $('#' + editor_id).addClass(settings.editor_class);  

            // set the css and content of the editor
            switch(settings.editor_tag){
                default:
                case 'input':
                    $('#' + editor_id).css('display', 'inline');
                    $('#' + editor_id).css('width', (w + 40));
                    $('#' + editor_id).val(content);
                break;
                case 'textarea':
                    $('#' + editor_id).css('display', 'inline');                
                    $('#' + editor_id).css('width', (w + 40));
                    $('#' + editor_id).css('height', (h + 8));
                    $('#' + editor_id).text(content);
                case 'select':
                    $('#' + editor_id).css('display', 'inline');                
                    $('#' + editor_id).css('width', (w + 40));
                    $('#' + editor_id).val(content);
                break;
            }

            // give the editor focus
            $('#' + editor_id).focus();

        });

        /**
        ---------------------------------------------------------------------------------------------------------------
        Handle Editor Blur
        ---------------------------------------------------------------------------------------------------------------
        */
        $(document).on('blur', '#' + editor_id, function(){
            
            // get some properties of the editor
            var content = $('#' + editor_id).val();

            // replace the editor with the element and apply classes
            $('#' + editor_id).replaceWith(tpl_element);
            $('#' + element_id).addClass(settings.element_class);

            // set the content of the element
            if(content.length < 1){
                content += (settings.element_placeholder_icon != '') ? '<img src="' + settings.element_placeholder_icon + '" class="editable-icon" />' : '';
                content += (settings.element_placeholder != '') ? settings.element_placeholder : '';
            }
            
            // if we are using an icon update element html otherwise update element text
            (settings.element_placeholder_icon != '') ? $('#' + element_id).html(content) : $('#' + element_id).text(content);

            // if a callback function is registered then fire it off
            (settings.callback_post != '') ? window[settings.callback_post](content) : '';

        });

        /**
        ---------------------------------------------------------------------------------------------------------------
        Element Expand
        ---------------------------------------------------------------------------------------------------------------
        */
        if(settings.editor_tag_expand == 1){
            $(document).on('keyup', '#' + editor_id, function(e){
                var ignored = new Array(
                    8,    // backspace
                    13,   // enter
                    16,   // shift
                    17,   // ctrl
                    18,   // alt
                    19,   // pause / break
                    20,   // caps lock
                    35,   // end
                    36,   // home
                    37,   // left arrow
                    38,   // up arrow
                    39,   // right arrow
                    40,   // down arrow
                    44,   // printscreen
                    45,   // insert
                    46,   // del
                    113,  // f2
                    115,  // f4
                    118,  // f7
                    119,  // f8
                    120,  // f9
                    145   // scroll lock
                );
                if(jQuery.inArray(e.which, ignored) == -1){
                    var w = $('#' + editor_id).width();
                    $('#' + editor_id).css('width', (w + 28));        
                }

            });  
        } 

        /**
        ---------------------------------------------------------------------------------------------------------------
        Main
        ---------------------------------------------------------------------------------------------------------------
        */

        // if detect no element content and using placeholder icon then set plugin optiona accordingly
        settings.auto_trigger = ($('#' + element_id).text() == '' && settings.element_placeholder_icon != '') ? 1 : 0;
        
        // if auto class then apply CSS class to the element
        if(settings.auto_class == 1){
            $('#' + element_id).addClass(settings.element_class);
        }

        // if auto trigger then trigger a click blur cycle on the element
        if(settings.auto_trigger == 1){
            $('#' + element_id).trigger('click');
            $('#' + editor_id).trigger('blur');
        }                      

    }; 
}(jQuery));